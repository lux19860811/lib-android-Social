package com.bookbuf.social.handlers.share.content.media;

import java.util.Map;

/**
 * Created by robert on 16/6/29.
 */
public interface IMediaObject {
	String toUrl ();

	IMediaObject.MediaType getMediaType ();

	boolean isUrlMedia ();

	Map<String, Object> toUrlExtraParams ();

	byte[] toByte ();

	boolean isMultiMedia ();

	enum MediaType {
		IMAGE {
			public String toString () {
				return "0";
			}
		},
		VIDEO {
			public String toString () {
				return "1";
			}
		},
		MUSIC {
			public String toString () {
				return "2";
			}
		},
		TEXT {
			public String toString () {
				return "3";
			}
		},
		TEXT_IMAGE {
			public String toString () {
				return "4";
			}
		},
		WEB_PAGE {
			public String toString () {
				return "5";
			}
		};

		MediaType () {
		}

		public static IMediaObject.MediaType convertToEnum (String s) {
			IMediaObject.MediaType[] mediaTypes = values ();
			int length = mediaTypes.length;

			for (int i = 0; i < length; ++i) {
				IMediaObject.MediaType mediaType = mediaTypes[i];
				if (mediaType.toString ().equals (s)) {
					return mediaType;
				}
			}

			return null;
		}
	}
}
