package com.bookbuf.social.handlers.share.content.media;

import android.os.Parcel;
import android.text.TextUtils;

/**
 * Created by robert on 16/6/29.
 */

public abstract class BaseMediaObject implements IMediaObject {
	public String mText = null;
	protected String mediaUrl = "";
	protected String title = "";
	protected String thumb = "";
	protected String targetUrl = "";
	protected String description = "";

	public BaseMediaObject () {
	}

	public BaseMediaObject (String mediaUrl) {
		this.mediaUrl = mediaUrl;
	}

	public String getDescription () {
		return this.description;
	}

	public void setDescription (String description) {
		this.description = description;
	}

	public String toUrl () {
		return this.mediaUrl;
	}

	public void setMediaUrl (String mediaUrl) {
		this.mediaUrl = mediaUrl;
	}

	public boolean isUrlMedia () {
		return !TextUtils.isEmpty (this.mediaUrl);
	}

	public String getTitle () {
		return this.title;
	}

	public void setTitle (String title) {
		this.title = title;
	}

	public String getThumb () {
		return this.thumb;
	}

	public void setThumb (String thumb) {
		this.thumb = thumb;
	}

	public void setTargetUrl (String targetUrl) {
		this.targetUrl = targetUrl;
	}

	public String getTargetUrl () {
		return this.targetUrl;
	}

	protected BaseMediaObject (Parcel parcel) {
		if (parcel != null) {
			this.mediaUrl = parcel.readString ();
			this.title = parcel.readString ();
			this.thumb = parcel.readString ();
			this.targetUrl = parcel.readString ();
		}

	}

	@Override
	public String toString () {
		return "BaseMediaObject{" +
				"mText='" + mText + '\'' +
				", mediaUrl='" + mediaUrl + '\'' +
				", title='" + title + '\'' +
				", thumb='" + thumb + '\'' +
				", targetUrl='" + targetUrl + '\'' +
				", description='" + description + '\'' +
				'}';
	}
}
