package com.bookbuf.social;

import com.bookbuf.social.platforms.IPlatform;
import com.bookbuf.social.platforms.WeChatPlatform;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by robert on 16/6/29.
 */
public class PlatformConfiguration {

	public static Map<PlatformEnum, IPlatform> configs = new HashMap<> ();

	static {
		configs.put (PlatformEnum.WX_SCENE, new WeChatPlatform (PlatformEnum.WX_SCENE));
		configs.put (PlatformEnum.WX_SCENE_TIMELINE, new WeChatPlatform (PlatformEnum.WX_SCENE_TIMELINE));
		configs.put (PlatformEnum.WX_SCENE_FAVORITE, new WeChatPlatform (PlatformEnum.WX_SCENE_FAVORITE));
	}

	public PlatformConfiguration () {
	}

	public static void setWeChat (final String appId, final String appSecret) {
		WeChatPlatform weChat = (WeChatPlatform) getPlatform (PlatformEnum.WX_SCENE);
		weChat.appId = appId;
		weChat.appSecret = appSecret;

		WeChatPlatform weChatFriendCircle = (WeChatPlatform) getPlatform (PlatformEnum.WX_SCENE_TIMELINE);
		weChatFriendCircle.appId = appId;
		weChatFriendCircle.appSecret = appSecret;

		WeChatPlatform weChatConversation = (WeChatPlatform) getPlatform (PlatformEnum.WX_SCENE_FAVORITE);
		weChatConversation.appId = appId;
		weChatConversation.appSecret = appSecret;
	}

	public static IPlatform getPlatform (PlatformEnum media) {
		return configs.get (media);
	}
}
