package com.bookbuf.social;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 分享的媒介
 */
public enum PlatformEnum {
	WX_SCENE ("com.bookbuf.wechat.WXSSOHandler"),
	WX_SCENE_TIMELINE ("com.bookbuf.wechat.WXSSOHandler"),
	WX_SCENE_FAVORITE ("com.bookbuf.wechat.WXSSOHandler");

	String handlerConfiguration;

	PlatformEnum (String handler) {
		this.handlerConfiguration = handler;
	}

	public String getHandlerConfiguration () {
		return handlerConfiguration;
	}

	@Deprecated
	public static PlatformEnum convertToEnum (String s) {
		if (TextUtils.isEmpty (s)) {
			return null;
		} else {
			PlatformEnum[] shareMedias = values ();
			int length = shareMedias.length;

			for (int i = 0; i < length; ++i) {
				PlatformEnum media = shareMedias[i];
				if (media.toString ().trim ().equals (s)) {
					return media;
				}
			}
			return null;
		}
	}

	private static List<PlatformEnum> platforms = new ArrayList<> ();

	public static List<PlatformEnum> getSupportPlatforms () {
		platforms.clear ();
		platforms.add (WX_SCENE);
		platforms.add (WX_SCENE_TIMELINE);
		platforms.add (WX_SCENE_FAVORITE);

		return platforms;
	}

}
