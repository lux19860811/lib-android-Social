package com.bookbuf.wechat.content;

import com.bookbuf.social.handlers.share.content.ShareContent;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXTextObject;

/**
 * Created by robert on 16/6/30.
 */
class WXText extends WXShareContent {

	protected WXText (ShareContent shareContent) {
		super (shareContent);
	}

	@Override
	public WXShareType getType () {
		return WXShareType.TEXT;
	}

	@Override
	public WXMediaMessage build () {
		WXTextObject textObj = new WXTextObject ();
		textObj.text = getShareContent ().mText;
		WXMediaMessage msg = new WXMediaMessage ();
		msg.mediaObject = textObj;
		msg.description = getShareContent ().mText;
		msg.title = getShareContent ().mTitle;
		return msg;
	}


}
