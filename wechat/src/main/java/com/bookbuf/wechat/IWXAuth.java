//package com.bookbuf.wechat;
//
///**
// * Created by robert on 16/6/30.
// */
//public interface IWXAuth {
//
//	/***
//	 * 第一步：请求CODE
//	 * 移动应用微信授权登录
//	 * 开发者需要配合使用微信开放平台提供的SDK进行授权登录请求接入。
//	 * 正确接入SDK后并拥有相关授权域（scope，什么是授权域？）权限后，
//	 * 开发者移动应用会在终端本地拉起微信应用进行授权登录，
//	 * 微信用户确认后微信将拉起开发者移动应用，
//	 * 并带上授权临时票据（code）。
//	 */
//	void requestCode (/*此处应有回调*/);
//
//
//	/***
//	 * 第二步：通过code获取access_token
//	 */
//	void requestAccessTokenByCode (/*此处应有回调*/);
//
//	/***
//	 * 第三步：通过access_token调用接口
//	 */
//	void callWXApi (/*接口适配器*/);
//}
